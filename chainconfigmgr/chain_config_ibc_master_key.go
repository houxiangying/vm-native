/*
Copyright (C) Beijing Advanced Innovation Center for Future Blockchain and Privacy Computing (未来区块链与隐
私计算⾼精尖创新中⼼). All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package chainconfigmgr

import (
	"fmt"
	"strings"

	configPb "chainmaker.org/chainmaker/pb-go/v2/config"
	"chainmaker.org/chainmaker/protocol/v2"
	"chainmaker.org/chainmaker/utils/v2"
	"chainmaker.org/chainmaker/vm-native/v2/common"
)

// ChainIBCMasterKeysRuntime chain ibc master key manager
type ChainIBCMasterKeysRuntime struct {
	log protocol.Logger
}

// MasterKeyAdd add master key
func (r *ChainIBCMasterKeysRuntime) MasterKeyAdd(txSimContext protocol.TxSimContext, params map[string][]byte) (
	result []byte, err error) {
	// [start]
	chainConfig, err := common.GetChainConfig(txSimContext)
	if err != nil {
		r.log.Error(err)
		return nil, err
	}

	orgId := string(params[paramNameOrgId])
	masterKeysStr := string(params[paramNameMasterKer])
	if utils.IsAnyBlank(orgId, masterKeysStr) {
		err = fmt.Errorf("%s, add master key require param [%s, %s] not found",
			common.ErrParams.Error(), paramNameOrgId, paramNameRoot)
		r.log.Error(err)
		return nil, err
	}
	masterKeys := strings.Split(masterKeysStr, ",")

	existOrgId := false
	for i := 0; i < len(chainConfig.IbcMasterKeys); i++ {
		if chainConfig.IbcMasterKeys[i].OrgId == orgId {
			existOrgId = true
			chainConfig.IbcMasterKeys[i].MasterKeys = append(chainConfig.IbcMasterKeys[i].MasterKeys, masterKeys...)
		}
	}

	if !existOrgId {
		masterKey := &configPb.IBCMasterKeyConfig{OrgId: orgId, MasterKeys: masterKeys}
		chainConfig.IbcMasterKeys = append(chainConfig.IbcMasterKeys, masterKey)
	}

	result, err = SetChainConfig(txSimContext, chainConfig)
	if err != nil {
		r.log.Errorf("master key add fail, %s, orgId[%s] cert[%s]", err.Error(), orgId, masterKeysStr)
	} else {
		r.log.Infof("master key add success. orgId[%s] cert[%s]", orgId, masterKeysStr)
	}
	return result, err
}